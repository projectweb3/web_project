import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Member } from '@/types/Member'

export const useMemberStore = defineStore('member', () => {
  const memberDialog = ref(false)
  const members = ref<Member[]>([
    {
    id: 1,
    name: 'Optimus Prime',
    tel: '0123456789',
    point: 100    }
  ])
  const tel = ref('')
  const currentMember = ref<Member | null >()
  const searchMember = (tel: string) => {
    const index = members.value.findIndex((item) => item.tel === tel)
    if(index < 0) {
      currentMember.value = null
    }
    currentMember.value = members.value[index]
  }
  function clear() {
    currentMember.value = null
  }

  function saveMember(mem: Member) {
    const newReceipt: Member = {
      id: mem.id,
      name: mem.name,
      tel: mem.tel,
      point: mem.point
    };
    members.value.push(newReceipt);
    clear();
    memberDialog.value = false
  }

    return {
    members, currentMember, saveMember,
    searchMember,  clear, tel
    }
})
