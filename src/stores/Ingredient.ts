import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Ingredient } from '@/types/Ingredient'

export const useIngredientStore = defineStore('ingredient', () => {
  const ingredients = ref<Ingredient[]>([
    {
        id: 1,
        IngredientID: 'P001',
        IngredientName: 'น้ำตาล(Sugar)',
        Minimum: 10,
        Balance: 25,
        Price: 40
      },
      {
        id: 2,
        IngredientID: 'P002',
        IngredientName: 'วิปครีม(whipped cream)',
        Minimum: 5,
        Balance: 12,
        Price: 289
      },
      {
        id: 3,
        IngredientID: 'P003',
        IngredientName: 'ผงชาเขียว(matcha powder)',
        Minimum: 10,
        Balance: 16,
        Price: 70
      }
  ])
  const currentIngredient = ref<Ingredient | null >()
  const searchIngredient = (name: string) => {
    const index = ingredients.value.findIndex((item) => item.IngredientName === name)
    if(index < 0) {
      currentIngredient.value = null
    }
    currentIngredient.value = ingredients.value[index]
  }
  function clear() {
    currentIngredient.value = null
  }

  function saveIngredient(ing: Ingredient){
    const newIngredient: Ingredient = {
    id: ing.id,
    IngredientID: ing.IngredientID,
    IngredientName: ing.IngredientName,
    Minimum: ing.Minimum,
    Balance: ing.Balance,
    Price: ing.Price
    };
    ingredients.value.push(newIngredient);
    clear();
  }
  
  return {
    ingredients, currentIngredient,
    searchIngredient,  clear, saveIngredient
    }
})