type Product = {
    id: number;
    name: string;
    price: number;
    level?: string;
    type: number;
}

export {type Product}